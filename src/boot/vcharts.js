
import { boot } from 'quasar/wrappers'
import Chart from "v-chart-plugin";


export default boot(({ app }) => {
  // for use inside Vue files (Options API) through this.$axios and this.$api

  app.config.globalProperties.$chart = Chart
})
