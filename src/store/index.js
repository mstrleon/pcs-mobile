
import { createStore } from 'vuex'

import app from "./app/index";
export default function (/* { ssrContext } */) {
  const Store = createStore({
    modules: {
      app
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEBUGGING
  });

  return Store;
}
