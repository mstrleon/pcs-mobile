export function SET_LOADING(state, loading) {
  state.loading = loading;
}

export function SET_BUILDINGS(state, buildings) {
  state.buildings = buildings;
}

export function SET_ROOMS(state, rooms) {
  state.rooms = rooms;
}
