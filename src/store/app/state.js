import { buildingsData } from "../../config/buildingsData";
export default function () {
  return {
    //
    loading: false,
    buildings: null,
    rooms: null
  };
}
