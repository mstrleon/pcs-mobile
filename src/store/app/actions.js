import { buildingsData, roomsData } from "../../config/buildingsData";
import {
  generateRoomsAmount,
  generateRoomData
} from "src/helpers/generateData";

export function getBuildingsData({ commit }) {
  const buildings = buildingsData;
  commit("SET_BUILDINGS", buildings);
}

export function getRoomsData({ commit }) {
  const roomsAmount = generateRoomsAmount();

  let rooms = [];
  for (let i = 0; i < roomsAmount + 1; i++) {
    rooms.push({
      rid: roomsData[i + 1].rid,
      name: roomsData[i + 1].name,
      data: generateRoomData()
    });
  }
  console.log(rooms);
  commit("SET_ROOMS", rooms);
}
