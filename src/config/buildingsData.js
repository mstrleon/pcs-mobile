export const buildingsData = {
  1: {
    bid: 1,
    name: "Building 1",
    floors: {
      1: {
        id: 1,
        name: "Ground Floor"
      },
      2: {
        id: 2,
        name: "First Floor"
      },
      3: {
        id: 3,
        name: "Second Floor"
      }
    }
  },
  2: {
    bid: 2,
    name: "Building 2",
    floors: {
      1: {
        id: 1,
        name: "Ground Floor"
      },
      2: {
        id: 2,
        name: "First Floor"
      },
      3: {
        id: 3,
        name: "Second Floor"
      },
      4: {
        id: 4,
        name: "Fourth Floor"
      }
    }
  },
  3: {
    bid: 3,
    name: "Building 3",
    floors: {
      1: {
        id: 1,
        name: "Ground Floor"
      }
    }
  }
};

export const roomsData = {
  1: {
    rid: 1,
    name: "Room 1"
  },
  2: {
    rid: 2,
    name: "Room 2"
  },
  3: {
    rid: 3,
    name: "Room 3"
  },
  4: {
    rid: 4,
    name: "Room 4"
  },
  5: {
    rid: 5,
    name: "Room 5"
  },
  6: {
    rid: 6,
    name: "Diner"
  },
  7: {
    rid: 7,
    name: "Rest zone"
  },
  8: {
    rid: 8,
    name: "Room 6"
  }
};

export const dataLimits = {
  wellness: {
    name: "Wellness",
    min: 0,
    max: 3
  },
  temperature: {
    name: "Temperature",
    min: 7,
    max: 45
  },
  humidity: {
    name: "Humidity",
    min: 0,
    max: 100
  },
  co2: {
    name: "CO2",
    min: 20,
    max: 1000
  },
  pm10: {
    name: "PM10",
    min: 0,
    max: 100
  },
  PM25: {
    name: "PM2.5",
    min: 0,
    max: 100
  },
  voc: {
    name: "VOC",
    min: 0,
    max: 300
  },
  rssi: {
    name: "RSSI",
    min: 0,
    max: 100
  },
  battery: {
    name: "Battery",
    min: 0,
    max: 100
  }
};
