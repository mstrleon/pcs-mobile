// default src/router/routes.js content:
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: "", component: () => import("pages/Index.vue") },
      {
        path: "buildings",
        component: () => import("pages/Buildings.vue")
      },
      {
        path: "buildings/:bid/floor/:id",
        component: () => import("pages/Floor.vue")
      }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
