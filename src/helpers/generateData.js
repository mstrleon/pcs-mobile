import { dataLimits, roomsData } from "../config/buildingsData";

function generateNum(min, max) {
  return Math.round((Math.random() * (max - min) + min) * 100) / 100;
}

export function generateRoomsAmount() {
  return Math.floor(generateNum(1, Object.keys(roomsData).length + 1));
}

function generateMonthData(min, max) {
  let dataArr = [];
  for (let i = 0; i < 31; i++) {
    dataArr.push(generateNum(min, max));
  }
  return dataArr;
}
function generateDayData(min, max) {
  let dataArr = [];
  for (let i = 0; i < 24; i++) {
    dataArr.push(generateNum(min, max));
  }
  return dataArr;
}

export function generateRoomData() {
  let roomData = {};
  for (let key in dataLimits) {
    const monthlyData = generateMonthData(
      dataLimits[key].min,
      dataLimits[key].max
    );
    const dayData = generateDayData(dataLimits[key].min, dataLimits[key].max);
    roomData[key] = {
      month: monthlyData,
      day: dayData,
      name: dataLimits[key].name,
      key: key
    };
  }
  return roomData;
}
export function prapareChartData(rooms, rid, param) {
  console.log("prapareChartData", rooms, rid, param);
  const room = rooms.find((r) => r.rid === rid);
  const chartData = room.data[param].month.map((val) => {
    return { count: val };
  });
  return chartData;
}
