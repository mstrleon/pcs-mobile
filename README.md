![Logo](public/pcs-neos-both.png)



# PCS-Mobile Dashboard

A IOT Data Visualisation Tool for Mobiles. 
Data is supposed to be received from different sensors located in schools around Thailand.
But currently dummy data is being generated.




## Features

- Mobile Version only
- Dummy data generation as default source of data
- Monthly / Daily Graphs
- Cross platform


## Demo

![Screenshot](public/pcs-mobile-screen.png)


https://pcs-stats.netlify.app 

This app is optimized for mobile only, since another version exist for desktop use (running on PowerBI).


## Installation

#### Just clone the project and use Yarn to install
```bash
yarn

```

#### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev

```

#### Lint the files
```bash
yarn run lint

```

#### Build the app for production
```bash
quasar build

```

#### Customize the configuration
See [Configuring quasar.config.js](https://quasar.dev/quasar-cli/quasar-conf-js).

    

###     
## Used By

This project is used by:

<img src="public/logo-bkk-prep.png" width="40%" height="40%" alt="Bangkok Prep">


## Author

- [Leonid Anufriev](https://www.gitlab.com/mstrleon)


## Tech Stack

**Client:** Vue3, Vuex, Quasar2, ApexChart

**Server:** Node

